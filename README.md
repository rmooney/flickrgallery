# README #

There is an intermittent problem where the JSON feed doesn't load properly. This is because the Flickr feed contains an incorrect escape sequence for author names that contain an apostrophe.

example:
`"author": "nobody@flickr.com (\"Rob\'s Photos\")",`

The iOS JSON parser throws an error when it encounters this. The app doesn't crash but it fails to load the feed.
