//
//  ImageRequest.swift
//  FlickrGallery
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import UIKit

/// Requests an image from a URL
class ImageRequest: NSObject {
    
    /// Called when the request is complete
    var completionBlock: ((UIImage?, Error?) -> ())?
    
    // MARK: - Methods
    
    init(url: URL) {
        receivedData = Data()
        super.init()
        session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        dataTask = session.dataTask(with: url)
    }
    
    func resume() {
        dataTask.resume()
    }
    
    func cancel() {
        dataTask.cancel()
    }
    
    // MARK: - Private properties
    
    private var session: URLSession!
    private var dataTask: URLSessionDataTask!
    private var receivedData: Data
    
}

// MARK: -

extension ImageRequest: URLSessionDataDelegate {
    
    // MARK: - URL session data delegate
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        receivedData.append(data)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        let image = UIImage(data: receivedData)
        completionBlock?(image, error)
    }
    
}
