//
//  ImageRequestManager.swift
//  FlickrGallery
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import UIKit

// MARK: - Methods

class ImageRequestManager {

    func requestImage(atUrl url: URL, completion: @escaping (UIImage?, Error?) -> ()) {
        if let cachedImage = cachedImagesByURL[url] {
            completion(cachedImage, nil)
            return
        }
        
        let imageRequest = ImageRequest(url: url)
        
        imageRequest.completionBlock = { [weak self] image, error in
            self?.cachedImagesByURL[url] = image
            completion(image, error)
        }
        
        imageRequest.resume()
        
    }

    // MARK: - Private properties

    private var cachedImagesByURL: [URL: UIImage] = [:]
}
