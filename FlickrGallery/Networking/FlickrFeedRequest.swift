//
//  FlickrFeedRequest.swift
//  FlickrGallery
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import Foundation

/// A request for a Flickr public feed
class FlickrFeedRequest: NSObject {
    
    // MARK: - Types
    
    /// Represents a HTTP level error such as a 500 error
    enum HTTPError: Error {
        case statusCode(Int)
    }
    
    // MARK: - Properties
    
    /// A list of tags to filter the request
    let tags: [String]
    
    /// Called when the request is complete
    var completionBlock: ((FlickrFeed?, Error?) -> ())?
    
    // MARK: - Methods
    
    init(tags: String...) {
        self.tags = tags
        receivedData = Data()
        super.init()
        session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
        
        var endpoint = URLComponents(string: "https://api.flickr.com/services/feeds/photos_public.gne")!
        
        endpoint.queryItems = [
            URLQueryItem(name: "format", value: "json"),
            URLQueryItem(name: "nojsoncallback", value: "1"),
            URLQueryItem(name: "tags", value: tags.joined(separator: ","))
        ]
        
        dataTask = session.dataTask(with: endpoint.url!)
    }
    
    /// Start the request
    func resume() {
        dataTask.resume()
    }
    
    /// Cancel the request
    func cancel() {
        dataTask.cancel()
    }
    
    // MARK: - Private properties
    
    private var session: URLSession!
    private var dataTask: URLSessionDataTask!
    private var receivedData: Data
    
}

// MARK: -

extension FlickrFeedRequest: URLSessionDataDelegate {
    
    // MARK: - URL session data delegate
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        receivedData.append(data)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard error == nil else {
            completionBlock?(nil, error)
            return
        }
        
        let httpResponse = task.response as! HTTPURLResponse
        
        if httpResponse.statusCode == 200 {
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                let flickrFeed = try decoder.decode(FlickrFeed.self, from: receivedData)
                
                completionBlock?(flickrFeed, nil)
            } catch {
                completionBlock?(nil, error)
            }
        } else {
            // a HTTP level error occured
            completionBlock?(nil, HTTPError.statusCode(httpResponse.statusCode))
        }
    }
}
