//
//  FlickrFeedViewController.swift
//  FlickrGallery
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import UIKit

class FlickrFeedViewController: UICollectionViewController {
    
    var flickrFeedRequest: FlickrFeedRequest!
    var flickrFeedDataProvider: FlickrFeedDataProvider!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        
        flickrFeedRequest = FlickrFeedRequest(tags: "flower")

        flickrFeedRequest.completionBlock = { flickrFeed, error in
            guard let flickrFeed = flickrFeed else {
                return
            }
            
            DispatchQueue.main.async {

                self.flickrFeedDataProvider = FlickrFeedDataProvider()
                self.flickrFeedDataProvider.flickrFeed = flickrFeed
                
                self.collectionView?.dataSource = self.flickrFeedDataProvider
                self.collectionView?.delegate = self.flickrFeedDataProvider
                self.collectionView?.prefetchDataSource = self.flickrFeedDataProvider
                
                self.collectionView?.reloadData()
            }
        }
        
        flickrFeedRequest.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    

}
