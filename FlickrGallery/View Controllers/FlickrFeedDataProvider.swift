//
//  FlickrFeedDataProvider.swift
//  FlickrGallery
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import UIKit

class FlickrFeedDataProvider: NSObject, UICollectionViewDataSource, UICollectionViewDataSourcePrefetching, UICollectionViewDelegate {
    
    var flickrFeed: FlickrFeed!
    
    override init() {
        super.init()
        imageRequestManager = ImageRequestManager()
        dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return flickrFeed.photos.count
    }

    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            let photo = flickrFeed.photos[indexPath.item]
            imageRequestManager.requestImage(atUrl: photo.mediaURL) { _, _ in }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "flickrPhotoCell", for: indexPath) as! FlickrPhotoCell
        
        let photo = flickrFeed.photos[indexPath.item]
        
        cell.mediaURL = photo.mediaURL
        
        cell.titleLabel.text = photo.title
        cell.photographerLabel.text = "by \(photo.photographer)"
        cell.publishedLabel.text = dateFormatter.string(from: photo.published)
        cell.photoView.image = nil
        
        imageRequestManager.requestImage(atUrl: photo.mediaURL) { image, error in
            DispatchQueue.main.async {
                guard cell.mediaURL == photo.mediaURL else { return }
                cell.photoView.image = image
            }
        }
        
        return cell
    }
    
    private var imageRequestManager: ImageRequestManager!
    private var dateFormatter: DateFormatter!
    
    
}
