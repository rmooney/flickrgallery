//
//  FlickrPhotoCell.swift
//  FlickrGallery
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import UIKit

class FlickrPhotoCell: UICollectionViewCell {
    
    var mediaURL: URL?
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var photographerLabel: UILabel!
    @IBOutlet weak var publishedLabel: UILabel!
    
}
