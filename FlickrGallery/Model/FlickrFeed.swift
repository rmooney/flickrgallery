//
//  FlickrFeed.swift
//  FlickrGallery
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import Foundation

class FlickrFeed: Decodable {
    
    var title: String
    var modified: Date
    var photos: [FlickrPhoto]
    
    init(title: String, modified: Date = Date(), photos: [FlickrPhoto] = []) {
        self.title = title
        self.modified = modified
        self.photos = photos
    }
    
}

extension FlickrFeed {
    
    enum CodingKeys: String, CodingKey {
        case title
        case modified
        case photos = "items"
    }
    
}
