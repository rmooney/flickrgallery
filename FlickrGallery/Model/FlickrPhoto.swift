//
//  FlickrPhoto.swift
//  FlickrGallery
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import Foundation

class FlickrPhoto: Decodable {
    
    var title: String
    var mediaURL: URL
    var photographer: String
    var published: Date
    
    init(title: String, mediaURL: URL, photographer: String, published: Date = Date()) {
        self.title = title
        self.mediaURL = mediaURL
        self.photographer = photographer
        self.published = published
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decode(String.self, forKey: .title)
        let photographerString = try values.decode(String.self, forKey: .photographer)
        photographer = String(photographerString.dropFirst(20).dropLast(2))
        
        published = try values.decode(Date.self, forKey: .published)
        
        let media = try values.nestedContainer(keyedBy: MediaKeys.self, forKey: .media)
        let mediumMediaURLString = try media.decode(String.self, forKey: .mediaURL)
        
        mediaURL = URL(string: FlickrPhoto.largeMediaURLString(fromMediaURLString: mediumMediaURLString))!
    }
    
    private static func largeMediaURLString(fromMediaURLString mediaURLString: String) -> String {
        return "\(mediaURLString.dropLast(6))_z.jpg" //  use a larger size Flickr image
    }
}

extension FlickrPhoto {
    
    enum CodingKeys: String, CodingKey {
        case title
        case media
        case photographer = "author"
        case published
    }
    
    enum MediaKeys: String, CodingKey {
        case mediaURL = "m"
    }
    
}
