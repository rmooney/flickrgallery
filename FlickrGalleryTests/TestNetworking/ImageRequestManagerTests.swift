//
//  ImageRequestManagerTests.swift
//  FlickrGalleryTests
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class ImageRequestManagerTests: XCTestCase {
    
    var sut: ImageRequestManager!
    
    override func setUp() {
        super.setUp()
        
        sut = ImageRequestManager()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testImageRequestManager_RequestsImagesProperly() {
        
        let expectation = self.expectation(description: "Request images")
        
        var requestedImage1: UIImage?
        var foundError1: Error?
        
        var requestedImage2: UIImage?
        var foundError2: Error?
        
        sut.requestImage(atUrl: URL(string: "https://farm5.staticflickr.com/4442/37171565612_8e8433cc6e_m.jpg")!) { image, error in
            requestedImage1 = image
            foundError1 = error
            
            if requestedImage2 != nil || foundError2 != nil {
                expectation.fulfill()
            }
        }
        
        sut.requestImage(atUrl: URL(string: "https://farm5.staticflickr.com/4391/37344420575_b8734b8832_m.jpg")!) { image, error in
            requestedImage2 = image
            foundError2 = error
            
            if requestedImage1 != nil || foundError1 != nil {
                expectation.fulfill()
            }
        }
        
        waitForExpectations(timeout: 10) { error in
            if let requestedImage1 = requestedImage1 {
                XCTAssertGreaterThan(requestedImage1.size.width, 0)
                XCTAssertGreaterThan(requestedImage1.size.height, 0)
            } else {
                XCTAssertNotNil(foundError1)
            }
            
            if let requestedImage2 = requestedImage2 {
                XCTAssertGreaterThan(requestedImage2.size.width, 0)
                XCTAssertGreaterThan(requestedImage2.size.height, 0)
            } else {
                XCTAssertNotNil(foundError2)
            }
        }
        
    }
    
}
