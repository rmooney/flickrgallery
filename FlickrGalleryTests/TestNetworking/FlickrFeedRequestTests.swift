//
//  FlickrFeedRequestTests.swift
//  FlickrGalleryTests
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class FlickrFeedRequestTests: XCTestCase {
    
    var sut: FlickrFeedRequest!
    
    override func setUp() {
        super.setUp()
        
        sut = FlickrFeedRequest(tags: "flower")
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFlickrFeedRequest_RequestsFeedProperly() {
        let expectation = self.expectation(description: "Request Flickr feed")
        
        var flickrFeed: FlickrFeed?
        var flickrError: Error?
        
        sut.completionBlock = { feed, error in
            flickrFeed = feed
            flickrError = error
            expectation.fulfill()
        }
        
        sut.resume()
        
        waitForExpectations(timeout: 10) { error in
            if let flickrFeed = flickrFeed {
                XCTAssertEqual(flickrFeed.title, "Recent Uploads tagged flower")
                XCTAssertGreaterThan(flickrFeed.photos.count, 0)
            } else {
                // a network or server error occurred
                XCTAssertNotNil(flickrError)
            }
        }
    }
    
    
}
