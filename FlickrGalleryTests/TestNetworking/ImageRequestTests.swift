//
//  ImageRequestTests.swift
//  FlickrGalleryTests
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class ImageRequestTests: XCTestCase {
    
    var sut: ImageRequest!
    
    override func setUp() {
        super.setUp()
        
        sut = ImageRequest(url: URL(string: "https://farm5.staticflickr.com/4442/37171565612_8e8433cc6e_m.jpg")!)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testImageRequest_RequestsImageProperly() {
        
        let expectation = self.expectation(description: "Request image")
        
        var requestedImage: UIImage?
        var foundError: Error?
        
        sut.completionBlock = { image, error in
            requestedImage = image
            foundError = error
            expectation.fulfill()
        }
        
        sut.resume()
        
        waitForExpectations(timeout: 10) { error in
            if let requestedImage = requestedImage {
                XCTAssertGreaterThan(requestedImage.size.width, 0)
                XCTAssertGreaterThan(requestedImage.size.height, 0)
            } else {
                // A network or server error occurred
                XCTAssertNotNil(foundError)
            }
        }
        
    }
    
    
}
