//
//  FlickrPhotoTests.swift
//  FlickrGalleryTests
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class FlickrPhotoTests: XCTestCase {
    
    var sut: FlickrPhoto!
    
    override func setUp() {
        super.setUp()
        let json = """
        {
            "title": "More stunning flowers from my first #theme.",
            "link": "https://www.flickr.com/photos/30486697@N03/37342456285/",
            "media": {"m":"https://farm5.staticflickr.com/4369/37342456285_585883ffce_m.jpg"},
            "date_taken": "2017-09-20T02:48:48-08:00",
            "description": "More stunning flowers from my first #theme.",
            "published": "2017-09-20T09:48:48Z",
            "author": "nobody@flickr.com (\\"Matthew Smart Photography\\")",
            "author_id": "30486697@N03",
            "tags": "ifttt instagram"
        }
        """.data(using: .utf8)!
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        sut = try! decoder.decode(FlickrPhoto.self, from: json)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testPhoto_HasCorrectTitle() {
        XCTAssertEqual(sut.title, "More stunning flowers from my first #theme.")
    }
    
    func testPhoto_HasCorrectMediaURL() {
        XCTAssertEqual(sut.mediaURL, URL(string: "https://farm5.staticflickr.com/4369/37342456285_585883ffce_z.jpg"))
    }
    
    func testPhoto_HasCorrectPhotographer() {
        XCTAssertEqual(sut.photographer, "Matthew Smart Photography")
    }
    
    func testPhoto_HasPublishedDate() {
        let dateComponents = DateComponents(year: 2017, month: 9, day: 20, hour: 9, minute: 48, second: 48)
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(secondsFromGMT:0)!
        XCTAssertEqual(sut.published, calendar.date(from: dateComponents))
    }
    
    func testPhoto_ManualInit() {
        let now = Date()
        let url = URL(string: "https://farm5.staticflickr.com/4369/37342456285_585883ffce_z.jpg")!
        let photo = FlickrPhoto(title: "More stunning flowers from my first #theme.", mediaURL: url, photographer: "Matthew Smart Photography", published: now)
        
        XCTAssertEqual(photo.title, "More stunning flowers from my first #theme.")
        XCTAssertEqual(photo.mediaURL, url)
        XCTAssertEqual(photo.photographer, "Matthew Smart Photography")
        XCTAssertEqual(photo.published, now)
    }
   
}
