//
//  FlickrFeedTests.swift
//  FlickrGalleryTests
//
//  Created by Robert Mooney on 20/09/2017.
//  Copyright © 2017 Robert Mooney. All rights reserved.
//

import XCTest
@testable import FlickrGallery

class FlickrFeedTests: XCTestCase {
    
    var sut: FlickrFeed!
    
    override func setUp() {
        super.setUp()
        let json = """
        {
            "title": "Uploads from everyone",
            "link": "https://www.flickr.com/photos/",
            "description": "Uploads from everyone",
            "modified": "2017-09-20T09:48:43Z",
            "generator": "https://www.flickr.com",
            "items": []
        }
        """.data(using: .utf8)!
        
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        sut = try! decoder.decode(FlickrFeed.self, from: json)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testFeed_HasCorrectTitle() {
        XCTAssertEqual(sut.title, "Uploads from everyone")
    }
    
    func testFeed_HasCorrectModifiedDate() {
        let dateComponents = DateComponents(year: 2017, month: 9, day: 20, hour: 9, minute: 48, second: 43)
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        XCTAssertEqual(sut.modified, calendar.date(from: dateComponents))
    }
    
    func testFlickrFeed_ManualInit() {
        let flickrFeed = FlickrFeed(title: "Uploads from everyone")
        
        XCTAssertEqual(flickrFeed.title, "Uploads from everyone")
        XCTAssertEqual(flickrFeed.photos.count, 0)
    }
    
}
